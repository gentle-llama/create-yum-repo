#!/usr/bin/env python3

import json
import sys

import koji

default_archs = [
    "aarch64",
    "x86_64",
    "noarch",
]
default_koji_mirror = "https://kojihub.stream.centos.org/kojihub"

koji_mirror = default_koji_mirror

def package_name_from_nvr(nvr):
    name, _, _ = nvr.rsplit("-", 2)
    return name

def search_build_id(build_id):
    print(f"Querying koji mirror with build_id={build_id}")
    packages = {}

    session = koji.ClientSession(koji_mirror, opts={"no_ssl_verify": True})
    find_packages = session.listRPMs(int(build_id))
    for package in find_packages:
        packages[package["name"]] = package["nvr"]

    return packages

def main():
    # nvr = NetworkManager-1.37.2-1.el9
    lockfile = sys.argv[1]
    search = sys.argv[2]

    # Search for packages from koji
    search_prefixes = {
        "build_id=": search_build_id
    }

    new_nvrs = {}
    for search_prefix in search_prefixes:
        if search.startswith(search_prefix):
            new_nvrs = search_prefixes[search_prefix](search[len(search_prefix):])
            break
    else:
        search_prefixes = "\n".join(search_prefixes)
        raise Exception(f"Search parameter must be one of the following:\n{search_prefixes}")

    print(new_nvrs)

    # Load lockfile
    with open(lockfile, "r") as packages_file:
        lf_data = json.load(packages_file)

    # Replace packages with newer found versions
    new_lf_data = {}
    for distro in lf_data:
        new_lf_data[distro] = {"common": []}

        for nvr in lf_data[distro]["common"]:
            package_name = package_name_from_nvr(nvr)
            if package_name in new_nvrs:
                new_lf_data[distro]["common"].append(new_nvrs[package_name])
            else:
                new_lf_data[distro]["common"].append(nvr)

        new_lf_data[distro]["arch"] = {}
        for arch in lf_data[distro]["arch"]:
            new_lf_data[distro]["arch"][arch] = []
            for nvr in lf_data[distro]["arch"][arch]:
                package_name = package_name_from_nvr(nvr)
                if package_name in new_nvrs:
                    new_lf_data[distro]["arch"][arch].append(new_nvrs[package_name])
                else:
                    new_lf_data[distro]["arch"][arch].append(nvr)

    # Output new lockfile
    with open(f"new.json", "w", encoding="utf-8") as f:                                                                   
        json.dump(new_lf_data, f, indent=4)

if __name__ == "__main__":
    main()
